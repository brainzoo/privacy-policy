import React from "react";

const NotFoundPage = () => (
  <div>
    <h1>404</h1>
    <p>The page you're looking for could not be found.</p>
  </div>
);

export default NotFoundPage;
